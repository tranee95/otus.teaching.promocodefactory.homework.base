﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeResponse
    {
        public EmployeeResponse()
        {
        }

        public EmployeeResponse(Employee employee)
        {
            Id = employee.Id;
            Email = employee.Email;
            Roles = employee.Roles.Select(x => new RoleItemResponse()
            {
                Name = x.Name,
                Description = x.Description
            }).ToList();
            FullName = employee.FullName;
            AppliedPromocodesCount = employee.AppliedPromocodesCount;
        }

        public Guid Id { get; private set; }
        public string FullName { get; set; }

        public string Email { get; set; }

        public List<RoleItemResponse> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}