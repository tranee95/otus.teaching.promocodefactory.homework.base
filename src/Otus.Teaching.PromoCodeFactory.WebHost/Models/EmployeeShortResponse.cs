﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeShortResponse
    {
        public EmployeeShortResponse()
        {
        }

        public EmployeeShortResponse(Employee employee)
        {
            Id = employee.Id;
            Email = employee.Email;
            FullName = employee.FullName;
        }

        public Guid Id { get; set; }
        
        public string FullName { get; set; }

        public string Email { get; set; }
    }
}