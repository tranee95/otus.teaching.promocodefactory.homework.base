﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();
            var result = employees.Select(x => new EmployeeShortResponse(x)).ToList();

            return result;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null) return NotFound();
            
            return new EmployeeResponse(employee);
        }

        /// <summary>
        /// Создание сотрудника
        /// </summary>
        /// <param name="employeeItem"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> Create([FromBody]EmployeeItemResponse employeeItem)
        {
            var employee = new Employee(employeeItem.FirstName, employeeItem.LastName, employeeItem.Email);

            var result = await _employeeRepository.AddAsync(employee);
            return new EmployeeResponse(result);
        }

        /// <summary>
        /// Изменение данных сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employeeItem"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> Update(Guid id, EmployeeItemResponse employeeItem)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            
            employee.FirstName = employeeItem.FirstName;
            employee.LastName = employeeItem.LastName;
            employee.Email = employeeItem.Email;
            
            var result = await _employeeRepository.UpdateAsync(id, employee);

            if (result is null) return NotFound();
            return new EmployeeResponse(result);
        }

        /// <summary>
        /// Удаление сотрудника по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult<bool>> Delete(Guid id)
        {
            var result = await _employeeRepository.DeleteAsync(id);
            if (!result) return NotFound();
            
            return Ok();
        }
    }
}