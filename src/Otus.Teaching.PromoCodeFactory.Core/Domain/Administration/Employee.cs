﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee: BaseEntity
    {
        public Employee()
        {
        }

        public Employee(string firstName, string lastName, string email, int appliedPromocodesCount = 0)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Roles = new List<Role>();
            AppliedPromocodesCount = appliedPromocodesCount;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public string Email { get; set; }
        public List<Role> Roles { get; set; }
        public int AppliedPromocodesCount { get; set; }
    }
}